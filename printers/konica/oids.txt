SNMP Code To Get Data Konica
Total Counters:

.1.3.6.1.4.1.18334.1.1.1.5.7.2.1.1.0 � Total
.1.3.6.1.4.1.18334.1.1.1.5.7.2.1.3.0 � Total Duplex
.1.3.6.1.4.1.18334.1.1.1.5.7.2.1.8.0 � Number of Originals
.1.3.6.1.4.1.18334.1.1.1.5.7.2.1.9.0 � Number of Prints

Copy Counters:
Total
.1.3.6.1.4.1.18334.1.1.1.5.7.2.2.1.5.2.1 � Full Color
.1.3.6.1.4.1.18334.1.1.1.5.7.2.2.1.5.1.1 � Black
LargeSize
.1.3.6.1.4.1.18334.1.1.1.5.7.2.2.1.7.1.1 � Black
.1.3.6.1.4.1.18334.1.1.1.5.7.2.2.1.7.2.1 � Full Color

Print Counters:
Total
.1.3.6.1.4.1.18334.1.1.1.5.7.2.2.1.5.2.2 � Full Color
.1.3.6.1.4.1.18334.1.1.1.5.7.2.2.1.5.1.2 � Black
Large Size
.1.3.6.1.4.1.18334.1.1.1.5.7.2.2.1.7.2.2 � Full Color
.1.3.6.1.4.1.18334.1.1.1.5.7.2.2.1.7.1.2 � Black

Scans/Fax
Scans Counters
Name/OID: .1.3.6.1.4.1.18334.1.1.1.5.7.2.1.5.0 � Total
Name/OID: .1.3.6.1.4.1.18334.1.1.1.5.7.2.3.1.6.1 � LargeSize

Color Toner: (percentage remaining)
.1.3.6.1.2.1.43.11.1.1.9.1.1 � Yellow
.1.3.6.1.2.1.43.11.1.1.9.1.2 � Magenta
.1.3.6.1.2.1.43.11.1.1.9.1.3 � Cyan
.1.3.6.1.2.1.43.11.1.1.9.1.4 � Black

General Serial Number
.1.3.6.1.2.1.43.5.1.1.17.1