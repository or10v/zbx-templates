--
-- WDMYCLOUDEX4.mib
--

-- Copyright (c) 2013 WD Inc.,
-- Module Name: WDMYCLOUDEX4.mib
-- Abstract:
-- Defines the WDMYCLOUDEX4 MIBs .
--

	WDMYCLOUDEX4-MIB DEFINITIONS ::= BEGIN


	IMPORTS
		OBJECT-TYPE, Integer32, enterprises,
		NOTIFICATION-TYPE
			FROM SNMPv2-SMI
		DisplayString
			FROM SNMPv2-TC;

--
--  OID definitions
--

	WD		OBJECT IDENTIFIER ::= { enterprises 5127}
	productID	OBJECT IDENTIFIER ::= { WD 1}
	projectID	OBJECT IDENTIFIER ::= { productID 1}
	modelID 	OBJECT IDENTIFIER ::= { projectID 1}
	submodelID	OBJECT IDENTIFIER ::= { modelID 1}
	nasAgent	OBJECT IDENTIFIER ::= { submodelID 1}


--
-- Node definitions
--
			wdmycloudex4AgentVer OBJECT-TYPE
				SYNTAX DisplayString
				MAX-ACCESS read-only
				STATUS current
				DESCRIPTION
					"Version information for the agent of SNMP of WDMYCLOUDEX4."
				::= { nasAgent 1 }

-- ============================================================================

--
-- system information definitions
--
			wdmycloudex4SoftwareVersion OBJECT-TYPE
				SYNTAX DisplayString
				MAX-ACCESS read-only
				STATUS current
				DESCRIPTION
					"The device software version."
				::= { nasAgent 2 }

			wdmycloudex4HostName OBJECT-TYPE
				SYNTAX DisplayString
				MAX-ACCESS read-only
				STATUS current
				DESCRIPTION
					"The device host name."
				::= { nasAgent 3 }


			wdmycloudex4FTPServer OBJECT-TYPE
				SYNTAX DisplayString
				MAX-ACCESS read-only
				STATUS current
				DESCRIPTION
					"Ftp Server status.
					1 : enable
					0 : disable
					"
				::= { nasAgent 5 }

			wdmycloudex4NetType OBJECT-TYPE
				SYNTAX DisplayString
				MAX-ACCESS read-only
				STATUS current
				DESCRIPTION
					"The Network type.
					0 : Workgroup
					1 : Active Directory"
				::= { nasAgent 6 }

			wdmycloudex4Temperature OBJECT-TYPE
				SYNTAX DisplayString
				MAX-ACCESS read-only
				STATUS current
				DESCRIPTION
					"The temperature of the system."
				::= { nasAgent 7 }

			wdmycloudex4FanStatus OBJECT-TYPE
				SYNTAX DisplayString
				MAX-ACCESS read-only
				STATUS current
				DESCRIPTION
					"The status of the fan0."
				::= { nasAgent 8 }

-- ============================================================================

			wdmycloudex4VolumeTable OBJECT-TYPE
			SYNTAX SEQUENCE OF Wdmycloudex4VolumeEntry
			MAX-ACCESS not-accessible
			STATUS current
			DESCRIPTION
				"A table of active volumes on the NAS device."
			::= { nasAgent 9 }

			wdmycloudex4VolumeEntry OBJECT-TYPE
			SYNTAX Wdmycloudex4VolumeEntry
			MAX-ACCESS not-accessible
			STATUS current
			DESCRIPTION
				"An entry in the volume table."
			INDEX { wdmycloudex4VolumeNum }
			::= { wdmycloudex4VolumeTable 1 }

			Wdmycloudex4VolumeEntry ::=
			SEQUENCE {
				wdmycloudex4VolumeNum
					Integer32,
				wdmycloudex4VolumeName
					DisplayString,
				wdmycloudex4VolumeFsType
					DisplayString,
				wdmycloudex4VolumeRaidLevel
					DisplayString,
				wdmycloudex4VolumeSize
					DisplayString,
				wdmycloudex4VolumeFreeSpace
					DisplayString
			 }

			wdmycloudex4VolumeNum OBJECT-TYPE
			SYNTAX Integer32
			MAX-ACCESS read-only
			STATUS current
			DESCRIPTION
				"Instance number of the volume entry."
			::= { wdmycloudex4VolumeEntry 1 }

			wdmycloudex4VolumeName OBJECT-TYPE
			SYNTAX DisplayString
			MAX-ACCESS read-only
			STATUS current
			DESCRIPTION
				"The name of the volume."
			::= { wdmycloudex4VolumeEntry 2 }

			wdmycloudex4VolumeFsType OBJECT-TYPE
			SYNTAX DisplayString
			MAX-ACCESS read-only
			STATUS current
			DESCRIPTION
				"The type of file system of the volume.
				Example : ext3 or ext4"
			::= { wdmycloudex4VolumeEntry 3 }

			wdmycloudex4VolumeRaidLevel OBJECT-TYPE
			SYNTAX DisplayString
			MAX-ACCESS read-only
			STATUS current
			DESCRIPTION
				"The RAID level of the volume. (JBOD SPANNING RAID0 RAID1 RAID5 RAID10 RAID5+SPARE)"
			::= { wdmycloudex4VolumeEntry 4 }

			wdmycloudex4VolumeSize OBJECT-TYPE
			SYNTAX DisplayString
			MAX-ACCESS read-only
			STATUS current
			DESCRIPTION
				"The size of the volume in bytes."
			::= { wdmycloudex4VolumeEntry 5 }

			wdmycloudex4VolumeFreeSpace OBJECT-TYPE
			SYNTAX DisplayString
			MAX-ACCESS read-only
			STATUS current
			DESCRIPTION
				"Free space on the volume in bytes."
			::= { wdmycloudex4VolumeEntry 6 }

-- ============================================================================

		wdmycloudex4DiskTable OBJECT-TYPE
			SYNTAX SEQUENCE OF Wdmycloudex4DiskEntry
			MAX-ACCESS not-accessible
			STATUS current
			DESCRIPTION
				"A table of physical disks attached to the NAS device."
			::= { nasAgent 10 }

		wdmycloudex4DiskEntry OBJECT-TYPE
			SYNTAX Wdmycloudex4DiskEntry
			MAX-ACCESS not-accessible
			STATUS current
			DESCRIPTION
				"An entry in the physical disk table."
			INDEX { wdmycloudex4DiskNum }
			::= { wdmycloudex4DiskTable 1 }

		Wdmycloudex4DiskEntry ::=
			SEQUENCE {
				wdmycloudex4DiskNum
					Integer32,
				wdmycloudex4DiskVendor
					DisplayString,
				wdmycloudex4DiskModel
					DisplayString,
				wdmycloudex4DiskSerialNumber
					DisplayString,
        		wdmycloudex4DiskTemperature
					DisplayString,
				wdmycloudex4DiskCapacity
					DisplayString
			 }

		wdmycloudex4DiskNum OBJECT-TYPE
			SYNTAX Integer32
			MAX-ACCESS read-only
			STATUS current
			DESCRIPTION
				"Instance number of the disk entry."
			::= { wdmycloudex4DiskEntry 1 }

		wdmycloudex4DiskVendor OBJECT-TYPE
			SYNTAX DisplayString
			MAX-ACCESS read-only
			STATUS current
			DESCRIPTION
				"The vendor of the disk drive."
			::= { wdmycloudex4DiskEntry 2 }

		wdmycloudex4DiskModel OBJECT-TYPE
			SYNTAX DisplayString
			MAX-ACCESS read-only
			STATUS current
			DESCRIPTION
				"The disk drive's model name."
			::= { wdmycloudex4DiskEntry 3 }

		wdmycloudex4DiskSerialNumber OBJECT-TYPE
			SYNTAX DisplayString
			MAX-ACCESS read-only
			STATUS current
			DESCRIPTION
				"The disk drive's serial number."
			::= { wdmycloudex4DiskEntry 4 }

		wdmycloudex4DiskTemperature OBJECT-TYPE
			SYNTAX DisplayString
			MAX-ACCESS read-only
			STATUS current
			DESCRIPTION
				"The centigrade temperature of this disk."
			::= { wdmycloudex4DiskEntry 5 }

		wdmycloudex4DiskCapacity OBJECT-TYPE
			SYNTAX DisplayString
			MAX-ACCESS read-only
			STATUS current
			DESCRIPTION
				"The capacity of the disk in GB."
			::= { wdmycloudex4DiskEntry 6 }


-- ============================================================================

		wdmycloudex4UPSTable OBJECT-TYPE
			SYNTAX SEQUENCE OF Wdmycloudex4UPSEntry
			MAX-ACCESS not-accessible
			STATUS current
			DESCRIPTION
				"A table of UPS attached to the NAS device."
			::= { nasAgent 11 }

		wdmycloudex4UPSEntry OBJECT-TYPE
			SYNTAX Wdmycloudex4UPSEntry
			MAX-ACCESS not-accessible
			STATUS current
			DESCRIPTION
				"An entry in the UPS table."
			INDEX { wdmycloudex4UPSNum }
			::= { wdmycloudex4UPSTable 1 }

		Wdmycloudex4UPSEntry ::=
			SEQUENCE {
				wdmycloudex4UPSNum
					Integer32,
				wdmycloudex4UPSMode
					DisplayString,
				wdmycloudex4UPSManufacturer
					DisplayString,
				wdmycloudex4UPSProduct
					DisplayString,
				wdmycloudex4UPSBatteryCharge
					DisplayString,
				wdmycloudex4UPSStatus
					DisplayString,
			 }

		wdmycloudex4UPSNum OBJECT-TYPE
			SYNTAX Integer32
			MAX-ACCESS read-only
			STATUS current
			DESCRIPTION
				"Instance number of the UPS entry."
			::= { wdmycloudex4UPSEntry 1 }

		wdmycloudex4UPSMode OBJECT-TYPE
			SYNTAX DisplayString
			MAX-ACCESS read-only
			STATUS current
			DESCRIPTION
				"The mode of the UPS "
			::= { wdmycloudex4UPSEntry 2 }

		wdmycloudex4UPSManufacturer OBJECT-TYPE
			SYNTAX DisplayString
			MAX-ACCESS read-only
			STATUS current
			DESCRIPTION
				"The manufacturer of the UPS."
			::= { wdmycloudex4UPSEntry 3 }

		wdmycloudex4UPSProduct OBJECT-TYPE
			SYNTAX DisplayString
			MAX-ACCESS read-only
			STATUS current
			DESCRIPTION
				"The product name of the UPS."
			::= { wdmycloudex4UPSEntry 4 }

		wdmycloudex4UPSBatteryCharge OBJECT-TYPE
			SYNTAX DisplayString
			MAX-ACCESS read-only
			STATUS current
			DESCRIPTION
				"The battery charge of the UPS."
			::= { wdmycloudex4UPSEntry 5 }

		wdmycloudex4UPSStatus OBJECT-TYPE
			SYNTAX DisplayString
			MAX-ACCESS read-only
			STATUS current
			DESCRIPTION
				"The status of this UPS."
			::= { wdmycloudex4UPSEntry 6 }



-- ============================================================================
--
--  Notifications
--

	notifyEvts OBJECT IDENTIFIER ::= { nasAgent 200 }

	notifyPasswdChanged        NOTIFICATION-TYPE
    STATUS      current
    DESCRIPTION
        "An indication that the Administrator's password has been changed."
    ::= { notifyEvts 1 }

    notifyFirmwareUpgraded     NOTIFICATION-TYPE
    STATUS      current
    DESCRIPTION
        "An indication that firmware has been upgraded."
    ::= { notifyEvts 2 }

    notifyNetworkChanged        NOTIFICATION-TYPE
    STATUS      current
    DESCRIPTION
        "An indication that the network settings has been changed."
    ::= { notifyEvts 3 }

    notifyTemperatureExceeded        NOTIFICATION-TYPE
    STATUS      current
    DESCRIPTION
        "An indication that system temperature has exceeded."
    ::= { notifyEvts 4 }

	notifyVolumeNum OBJECT-TYPE
        SYNTAX Integer32
        MAX-ACCESS read-only
        STATUS current
        DESCRIPTION
            "Instance number of the volume entry."
        ::= { notifyEvts 200 }
    
    notifyUPSBatteryCharge OBJECT-TYPE
        SYNTAX Integer32
        MAX-ACCESS read-only
        STATUS current
        DESCRIPTION
            "The battery charge percentage."
        ::= { notifyEvts 201 }
    
    notifyBayNum OBJECT-TYPE
        SYNTAX Integer32
        MAX-ACCESS read-only
        STATUS current
        DESCRIPTION
            "The bay number."
        ::= { notifyEvts 202 }
		
	notifyPercent OBJECT-TYPE
        SYNTAX Integer32
        MAX-ACCESS read-only
        STATUS current
        DESCRIPTION
            "Percentage of completion."
        ::= { notifyEvts 203 }
	
	notifyMinutes OBJECT-TYPE
        SYNTAX Integer32
        MAX-ACCESS read-only
        STATUS current
        DESCRIPTION
            "Estimated time of completion."
        ::= { notifyEvts 204 }
    
	notifyPowerSupportNum OBJECT-TYPE
        SYNTAX Integer32
        MAX-ACCESS read-only
        STATUS current
        DESCRIPTION
            "Instance number of the power support entry."
        ::= { notifyEvts 205 }
		
	notifyInterfaceNum OBJECT-TYPE
        SYNTAX Integer32
        MAX-ACCESS read-only
        STATUS current
        DESCRIPTION
            "Instance number of the network interface."
        ::= { notifyEvts 206 }
	
	notifyVendor OBJECT-TYPE
		SYNTAX DisplayString
		MAX-ACCESS read-only
		STATUS current
		DESCRIPTION
			"The vendor name of the USB device."
		::= { notifyEvts 210 }
		
	notifyModel OBJECT-TYPE
		SYNTAX DisplayString
		MAX-ACCESS read-only
		STATUS current
		DESCRIPTION
			"The model of the USB device."
		::= { notifyEvts 211 }
		
	notifySerialNumber OBJECT-TYPE
		SYNTAX DisplayString
		MAX-ACCESS read-only
		STATUS current
		DESCRIPTION
			"The serial number of the USB device."
		::= { notifyEvts 212 }
		
	notifyFileSystem OBJECT-TYPE
		SYNTAX DisplayString
		MAX-ACCESS read-only
		STATUS current
		DESCRIPTION
			"The file system of the USB device."
		::= { notifyEvts 213 }

	notifyLabel OBJECT-TYPE
		SYNTAX DisplayString
		MAX-ACCESS read-only
		STATUS current
		DESCRIPTION
			"The label of the USB device."
		::= { notifyEvts 214 }
		
	notifyUserName OBJECT-TYPE
		SYNTAX DisplayString
		MAX-ACCESS read-only
		STATUS current
		DESCRIPTION
			"User name."
		::= { notifyEvts 215 }

    notifySystemOverTemperature        NOTIFICATION-TYPE
        STATUS      current
        DESCRIPTION
            "The system temperature is above the specific maximum temperature."
        ::= { notifyEvts  101}

    notifyVolumeUsageIsAbove95Percent        NOTIFICATION-TYPE
		OBJECTS { notifyVolumeNum }
        STATUS      current
        DESCRIPTION
            "The volume usage is above 95%."
        ::= { notifyEvts  102}
		
    notifySystemUnderTemperature        NOTIFICATION-TYPE
        STATUS      current
        DESCRIPTION
            "The system temperature is below the specific minimum temperature."
        ::= { notifyEvts 103}

    notifyNewFirmwareAvailable        NOTIFICATION-TYPE
        STATUS      current
        DESCRIPTION
            "A new version of firmware has been released."
        ::= { notifyEvts  104}

    notifyDriveSMARTfailure        NOTIFICATION-TYPE
        OBJECTS { notifyBayNum }
        STATUS      current
        DESCRIPTION
            "Drive self-check failed."
        ::= { notifyEvts  105}
		
    notifyFirmwareUpdateFailed        NOTIFICATION-TYPE
        STATUS      current
        DESCRIPTION
            "The firmware update failed."
        ::= { notifyEvts  106}

    notifyTemperatureNormal        NOTIFICATION-TYPE
        STATUS      current
        DESCRIPTION
            "The system temperature is within the normal specified temperature range."
        ::= { notifyEvts  107}
		
    notifyVolumeFailure        NOTIFICATION-TYPE
        OBJECTS { notifyVolumeNum }
        STATUS      current
        DESCRIPTION
            "The data volume on the drive is not accessible."
        ::= { notifyEvts  108}
		
    notifyFirmwareUpdateSuccessful        NOTIFICATION-TYPE
        STATUS      current
        DESCRIPTION
            "The firmware update was successful."
        ::= { notifyEvts  109}
		
    notifyPendingThermalshutdown        NOTIFICATION-TYPE
        STATUS      current
        DESCRIPTION
            "Your device is overheated and will shut down automatically in 1 hour if no action is taken."
        ::= { notifyEvts  110}

    notifyFactoryRestoreSucceeded        NOTIFICATION-TYPE
        STATUS      current
        DESCRIPTION
            "Your system is restored to its factory defaults."
        ::= { notifyEvts  111}
    
    notifySystemShuttingDown        NOTIFICATION-TYPE
        STATUS      current
        DESCRIPTION
            "The system is shutting down."
        ::= { notifyEvts  112}
		
    notifyPowerSupplyFailure        NOTIFICATION-TYPE
		OBJECTS { notifyPowerSupportNum }
        STATUS      current
        DESCRIPTION
            "Power supply failed."
        ::= { notifyEvts  113}
		
    notifyDownloadingFirmwareUpdate        NOTIFICATION-TYPE
        STATUS      current
        DESCRIPTION
            "Downloading firmware update."
        ::= { notifyEvts  114}

    notifyInstallingFirmwareUpdate        NOTIFICATION-TYPE
        STATUS      current
        DESCRIPTION
            "Installing firmware update."
        ::= { notifyEvts  115}
    
    notifyRebootRequired        NOTIFICATION-TYPE
        STATUS      current
        DESCRIPTION
            "Reboot required."
        ::= { notifyEvts  116}
		
    notifyFanNotWorking        NOTIFICATION-TYPE
        STATUS      current
        DESCRIPTION
            "The system fan is not working."
        ::= { notifyEvts  117}
		
    notifyOnUPSPower        NOTIFICATION-TYPE
        OBJECTS { notifyUPSBatteryCharge }
        STATUS      current
        DESCRIPTION
            "On UPS Power"
        ::= { notifyEvts  118}

    notifySystemIsInStandbyMode        NOTIFICATION-TYPE
        STATUS      current
        DESCRIPTION
            "System is in standby mode."
        ::= { notifyEvts  119}
		
    notifySystemRebooting        NOTIFICATION-TYPE
        STATUS      current
        DESCRIPTION
            "System Rebooting."
        ::= { notifyEvts  120}

    notifyFileSystemCheckFailed        NOTIFICATION-TYPE
        STATUS      current
        DESCRIPTION
            "Scan Disk has detected file system errors on your drive configuration."
        ::= { notifyEvts  121}

    notifySystemNotReady        NOTIFICATION-TYPE
        STATUS      current
        DESCRIPTION
            "The system is busy."
        ::= { notifyEvts  122}

    notifyStorageAlmostFull        NOTIFICATION-TYPE
		OBJECTS { notifyUsername }
        STATUS      current
        DESCRIPTION
            "Storage almost full"
        ::= { notifyEvts  123}
		
    notifyStorageLimitReached        NOTIFICATION-TYPE
		OBJECTS { notifyUsername }
        STATUS      current
        DESCRIPTION
            "Storage limit reached"
        ::= { notifyEvts  124}
		
    notifyUnsupportedUPSDevice        NOTIFICATION-TYPE
        STATUS      current
        DESCRIPTION
            "Unsupported UPS device"
        ::= { notifyEvts  125}
		
    notifyEthernetConnectedAt10MOr100M        NOTIFICATION-TYPE
		OBJECTS { notifyInterfaceNum }
        STATUS      current
        DESCRIPTION
            "Connection speed will be limited due to connecting at 10/100 Mbps Ethernet."
        ::= { notifyEvts  126}
		
    notify50PercentUPSPowerLeft        NOTIFICATION-TYPE
        STATUS      current
        DESCRIPTION
            "The system is running on UPS power with 50% power left."
        ::= { notifyEvts  127}
    
    notify15PercentUPSPowerLeft        NOTIFICATION-TYPE
        STATUS      current
        DESCRIPTION
            "The system is running on UPS power with 15% power left."
        ::= { notifyEvts  128}
    
    notifyUPSOutofPower        NOTIFICATION-TYPE
        STATUS      current
        DESCRIPTION
            "The UPS power is running out."
        ::= { notifyEvts  129}		

    notifyFileSystemErrorCorrected        NOTIFICATION-TYPE
        STATUS      current
        DESCRIPTION
            "File system check has detected and corrected errors on your configuration."
        ::= { notifyEvts  130}		
		
    notifyUnsupportedFileSystem        NOTIFICATION-TYPE
		OBJECTS { notifyVendor, notifyModel, notifySerialNumber, notifyFileSystem, notifyLabel }
        STATUS      current
        DESCRIPTION
            "An unsupported file system has been detected on a USB device."
        ::= { notifyEvts  131}
		
    notifyUnsupportedDrive        NOTIFICATION-TYPE
		OBJECTS { notifyBayNum }
        STATUS      current
        DESCRIPTION
            "Unsupported Drive."
        ::= { notifyEvts  132}
		
    notifyDriveFailed        NOTIFICATION-TYPE
        OBJECTS { notifyBayNum }
        STATUS      current
        DESCRIPTION
            "Drive failed."
        ::= { notifyEvts  133}
    
    notifyDriveAboutToFail        NOTIFICATION-TYPE
        OBJECTS { notifyBayNum }
        STATUS      current
        DESCRIPTION
            "Drive is about to fail."
        ::= { notifyEvts  134}
    
    notifyVolumeDegraded        NOTIFICATION-TYPE
        OBJECTS { notifyVolumeNum }
        STATUS      current
        DESCRIPTION
            "Volume Degraded."
        ::= { notifyEvts  135}
		
    notifyVolumeMigration        NOTIFICATION-TYPE
        OBJECTS { notifyVolumeNum, notifyPercent }
        STATUS      current
        DESCRIPTION
            "Volume migration."
        ::= { notifyEvts  136}
		
    notifyVolumeRebuilding        NOTIFICATION-TYPE
        OBJECTS { notifyVolumeNum , notifyMinutes }
        STATUS      current
        DESCRIPTION
            "Volume Rebuilding."
        ::= { notifyEvts  137}
		
    notifyVolumeRebuildFailed        NOTIFICATION-TYPE
        OBJECTS { notifyVolumeNum }
        STATUS      current
        DESCRIPTION
            "The volume rebuild has failed."
        ::= { notifyEvts  138}
    
    notifyVolumeExpansionFailed        NOTIFICATION-TYPE
        OBJECTS { notifyVolumeNum }
        STATUS      current
        DESCRIPTION
            "The volume expansion failed."
        ::= { notifyEvts  139}

    notifyRAIDRoamingEnabled        NOTIFICATION-TYPE
        STATUS      current
        DESCRIPTION
            "RAID roaming enabled."
        ::= { notifyEvts  140}

    notifyVolumeIsFormatted        NOTIFICATION-TYPE
		OBJECTS { notifyVolumeNum }
        STATUS      current
        DESCRIPTION
            "Volume is formatted."
        ::= { notifyEvts  141}		

    notifyExpandingVolumeProgress        NOTIFICATION-TYPE
		OBJECTS { notifyVolumeNum, notifyPercent }
        STATUS      current
        DESCRIPTION
            "Volume expanding."
        ::= { notifyEvts  142}

    notifyMediaScanStopped        NOTIFICATION-TYPE
        STATUS      current
        DESCRIPTION
            "The device stopped scanning your media. As a result, some thumbnails will not display when you access it remotely. To resume scanning, please restart your WD My Cloud device."
        ::= { notifyEvts  143}

    notifyReplaceDrive        NOTIFICATION-TYPE
        STATUS      current
        DESCRIPTION
            "Replace drive with blinking red LED."
        ::= { notifyEvts  144}

    notifyHighSystemTemperature        NOTIFICATION-TYPE
        STATUS      current
        DESCRIPTION
            "Your device is currently overheated. Ensure that your device has adequate ventilation."
        ::= { notifyEvts  145}
		
	notifyRestoreConfigFailed        NOTIFICATION-TYPE
        STATUS      current
        DESCRIPTION
            "Restoring configuration failed."
        ::= { notifyEvts  146}
	
	notifyStorageBelowThreshold        NOTIFICATION-TYPE
        STATUS      current
        DESCRIPTION
            "Storage below threshold."
        ::= { notifyEvts  147}

	notifyDriveInserted        NOTIFICATION-TYPE
		OBJECTS { notifyBayNum }
        STATUS      current
        DESCRIPTION
            "A new drive has been inserted into Bay."
        ::= { notifyEvts  148}

	notifyDriveRemoved        NOTIFICATION-TYPE
		OBJECTS { notifyBayNum }
        STATUS      current
        DESCRIPTION
            "Drive Removed from Bay."
        ::= { notifyEvts  149}

	notifyNoDrivesInstalled        NOTIFICATION-TYPE
        STATUS      current
        DESCRIPTION
            "There are no drives installed. Add drives to start using your system."
        ::= { notifyEvts  150}

	notifyHotSpareDriveAdded        NOTIFICATION-TYPE
        STATUS      current
        DESCRIPTION
            "Hot Spare drive added into RAID Array."
        ::= { notifyEvts  151}

	notifyRAIDMigrationCompleted        NOTIFICATION-TYPE
        STATUS      current
        DESCRIPTION
            "RAID migration completed."
        ::= { notifyEvts  152}

	notifyRAIDRebuildCompleted        NOTIFICATION-TYPE
        STATUS      current
        DESCRIPTION
            "RAID rebuild completed."
        ::= { notifyEvts  153}

	notifyNonWDRedNASDriveInserted        NOTIFICATION-TYPE
        STATUS      current
        DESCRIPTION
            "Non-WD Red NAS Drive inserted."
        ::= { notifyEvts  154}

	notifyNetworkLinkDown        NOTIFICATION-TYPE
		OBJECTS { notifyInterfaceNum }
        STATUS      current
        DESCRIPTION
            "The network link is down or has become intermittent."
        ::= { notifyEvts  155}	





--              ******************************************************

--              ******************************************************


	END

--
-- WDMYCLOUDEX4.mib
--
